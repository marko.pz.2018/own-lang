#include "unaryexprassion.h"


Exprassion *UnaryExprassion::getExpr() const
{
    return expr;
}

char UnaryExprassion::getOperation() const
{
    return operation;
}

UnaryExprassion::UnaryExprassion(char operation, Exprassion *expr){
    this->expr = expr;
    this->operation = operation;
}

Value* UnaryExprassion::eval(){
    double val = expr->eval()->asNumber();
    switch (operation) {
    case '-': return new NumberValue(val);
    case '+':
    default:return new NumberValue(val);
    }
}

void UnaryExprassion::toString(QTreeWidget* AST,QTreeWidgetItem* father){
    QTreeWidgetItem* operationItem = new QTreeWidgetItem;
    operationItem->setText(0,QString(operation));
    if(father != nullptr){
        father->addChild(operationItem);
    }
    else{
        AST->addTopLevelItem(operationItem);
    }
    expr->toString(AST,operationItem);

}
