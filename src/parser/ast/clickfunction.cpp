#include "clickfunction.h"

ClickFunction::ClickFunction()
{

}

Value *ClickFunction::execute(QList<Value *> args){
    if(args.size() < 2){
        throw std::runtime_error("No matching function");
    }
    if(args.size() % 2 != 0){
        throw std::runtime_error("No matching function");
    }
    ClickThread* thread = new ClickThread(args);
    thread->start();
    return new StringValue("Click ok");
}
