#include "runfunction.h"

RunFunction::RunFunction()
{

}

Value *RunFunction::execute(QList<Value *> args){
    if(args.length() != 1){
        throw std::runtime_error("No matching function");
    }
    QStringList arguments;
    arguments << "/c" << args[0]->asString();
    QProcess process;
    process.execute("cmd.exe",arguments);
}
