#include "dragthread.h"


DragThread::DragThread(QList<Value *> args){
    this->args = args;
}

void DragThread::run(){
    for(int pos = 0; pos < args.length() - 1; pos+=2){
        Sleep(3000);
        DWORD x = args[pos]->asNumber();
        DWORD y = args[pos + 1]->asNumber();
        mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE,x,y,0,0);
        Sleep(3000);
    }
    qDebug() << "Click done";
}
