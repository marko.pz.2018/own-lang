#pragma once
#ifndef EXPRASSION_H
#define EXPRASSION_H
#include "node.h"
#include <QString>
#include "value.h"
#include <QTreeWidget>
#include "functions.h"
#include "variables.h"

 class Exprassion : public Node
{
public:
    Exprassion();
    virtual Value* eval() = 0;
    virtual void toString(QTreeWidget*,QTreeWidgetItem*) = 0;
 };




#endif // EXPRASSIN_H
