#include "functionsthreads.h"


ClickThread::ClickThread(QList<Value *> args){
    this->args = args;
}

void ClickThread::run(){
    for(int pos = 0; pos < args.length() - 1; pos+=2){
        Sleep(2000);
        DWORD x = args[pos]->asNumber();
        DWORD y = args[pos + 1]->asNumber();
        mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_MOVE,x,y,0,0);
        mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP| MOUSEEVENTF_MOVE,x,y,0,0);
        Sleep(2000);
    }
    qDebug() << "Click done";
}

DragThread::DragThread(QList<Value *> args){
    this->args = args;
}

void DragThread::run(){
    for(int pos = 0; pos < args.length() - 1; pos+=2){
        Sleep(3000);
        DWORD x = args[pos]->asNumber();
        DWORD y = args[pos + 1]->asNumber();
        mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE,x,y,0,0);
        Sleep(3000);
    }
    qDebug() << "Click done";
}


pressThread::pressThread(QList<Value *> args){
    this->args = args;
}

void pressThread::run(){
    int keyCode;
    foreach(Value* arg , args){
        Sleep(2000);
        keyCode = arg->asNumber();
        keybd_event(keyCode,0,0,0);
        Sleep(2000);
    }
    qDebug("Press end");
}
