#pragma once
#ifndef BREAKSTATEMENT_H
#define BREAKSTATEMENT_H
#include "statement.h"

class BreakStatement : public Statement, public std::exception
{
public:
    BreakStatement();

    // Statement interface
public:
    void execute();
    void toString(QTreeWidget* AST,QTreeWidgetItem* father);
    //Node interface
    Node* accept(Visitor* visit){
        return this;
    }
};

#endif // BREAKSTATEMENT_H
