#include "variables.h"

Variables::Variables()
{

}

double Variables::get(QString key){
        return variables[key];

}

bool Variables::isExist(QString key){
    return variables.contains(key);
}

void Variables::set(QString key, double val){
    variables.insert(key,val);
}

QMap<QString,double>Variables::variables;
