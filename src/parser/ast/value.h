#pragma once
#ifndef VALUE_H
#define VALUE_H
#include <QString>

class Value
{
public:
    Value();
    virtual double asNumber() = 0;
    virtual QString asString() = 0;
};

class NumberValue : public Value
{
    double value;

    // Value interface
public:
    NumberValue(double value);
    double asNumber();
    QString asString();

};

class StringValue : public Value
{
    QString value;

public:

    StringValue(QString value);

    // Value interface
    double asNumber();
    QString asString();
};

#endif // VALUE_H

