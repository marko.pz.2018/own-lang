#include "pressfunction.h"


PressFunction::PressFunction()
{

}

Value *PressFunction::execute(QList<Value *> args){
    if(args.length() == 0){
        throw std::runtime_error("No matching function for call");
    }
    pressThread* PressThread = new pressThread(args);
    PressThread->start();
    return new StringValue("Press ok");
}
