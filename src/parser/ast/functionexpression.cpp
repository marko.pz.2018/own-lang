#include "functionexpression.h"


QList<Exprassion *> FunctionExpression::getArguments() const
{
    return arguments;
}

void FunctionExpression::setArguments(const QList<Exprassion *> &value)
{
    arguments = value;
}

void FunctionExpression::setArgument(int i,Exprassion* expr)
{
    arguments[i] = expr;
}

FunctionExpression::FunctionExpression(QString name){
    this->name = name;
}

FunctionExpression::FunctionExpression(QString name, QList<Exprassion *> arguments){
    this->name = name;
    this->arguments = arguments;
}

void FunctionExpression::addArgument(Exprassion *arg){
    arguments.append(arg);
}

Value *FunctionExpression::eval(){
    QList<Value*> values;
    foreach(Exprassion* arg, arguments){
        values.push_back(arg->eval());
    }
    Value* retur = Functions::get(name)->execute(values);
    return retur;
}

void FunctionExpression::toString(QTreeWidget *AST, QTreeWidgetItem *father){
    QTreeWidgetItem* funcItem = new QTreeWidgetItem;
    funcItem->setText(0,"name (" + name + ")");
    father->addChild(funcItem);

    foreach(Exprassion* arg, arguments){
        arg->toString(AST,father);
    }
}
