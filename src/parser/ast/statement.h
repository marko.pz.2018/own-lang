#pragma once
#ifndef STATEMENT_H
#define STATEMENT_H
#include <QString>
#include <QTreeWidget>
#include "exprassion.h"
#include "variables.h"
#include "node.h"

class Statement : public Node
{
public:
    Statement();
    virtual void execute() = 0;
    virtual void toString(QTreeWidget*,QTreeWidgetItem* = nullptr) = 0;
};



#endif // STATEMENT_H
