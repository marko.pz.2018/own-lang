#pragma once
#ifndef FUNCTIONSTATEMENT_H
#define FUNCTIONSTATEMENT_H
#include "statement.h"
#include "functionexpression.h"
class FunctionStatement : public Statement
{
private:
    FunctionExpression* func;
public:
    FunctionStatement(FunctionExpression* func);
    // Statement interface
public:
    void execute();
    void toString(QTreeWidget* AST,QTreeWidgetItem* father);

    // Node interface
    Node* accept(Visitor* visitor){
        return visitor->visit(this);
    }
    FunctionExpression *getFunc() const;
    void setFunc(Exprassion *value);
};

#endif // FUNCTIONSTATEMENT_H
