#include "clickthread.h"


ClickThread::ClickThread(QList<Value *> args){
    this->args = args;
}

void ClickThread::run(){
    for(int pos = 0; pos < args.length() - 1; pos+=2){
        Sleep(2000);
        DWORD x = args[pos]->asNumber();
        DWORD y = args[pos + 1]->asNumber();
        mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_MOVE,x,y,0,0);
        mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP| MOUSEEVENTF_MOVE,x,y,0,0);
        Sleep(2000);
    }
    qDebug() << "Click done";
}
