#include "functions.h"

Functions::Functions()
{

}

bool Functions::isExist(QString name){
    return functions.contains(name);
}

Function *Functions::get(QString name){
    if(isExist(name)){
        return functions[name];
    }
}

void Functions::put(QString name, Function *func){
    functions.insert(name,func);
}

QMap<QString,Function*> Functions::functions;


