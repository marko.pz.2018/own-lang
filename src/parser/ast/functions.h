#pragma once
#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <QMap>
#include "function.h"
#include "value.h"





class Functions
{
private:
    static QMap<QString,Function*> functions;

public:
    Functions();

    static bool isExist(QString name);

    static Function* get(QString name);

    static void put(QString name,Function* func);


};

#endif // FUNCTIONS_H
