#pragma once
#ifndef NUMBEREXPRESSION_H
#define NUMBEREXPRESSION_H
#include "exprassion.h"

class NumberExprassion: public Exprassion
{
private:
     double value;
public:
    NumberExprassion(double value);

    Value* eval() override;



        // Exprassion interface
public:
    void toString(QTreeWidget* , QTreeWidgetItem*);


    // Node interface
    Node* accept(Visitor* visitor){
        return this;
    }
};

#endif // NUMBEREXPRESSION_H
