#pragma once
#ifndef NODE_H
#define NODE_H

#include "optmizator/visitor.h"

class Node
{
public:
    Node();
    virtual Node* accept(Visitor*) = 0;
};

#endif // NODE_H
