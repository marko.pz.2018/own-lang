#include "ifelsestatement.h"


Statement *IfElseStatement::getIfStatement() const
{
    return ifStatement;
}

Statement *IfElseStatement::getElseStatement() const
{
    return elseStatement;
}

Exprassion *IfElseStatement::getExpr() const
{
    return expr;
}

void IfElseStatement::setIfStatement(Statement *value)
{
    ifStatement = value;
}

void IfElseStatement::setElseStatement(Statement *value)
{
    elseStatement = value;
}

void IfElseStatement::setExpr(Exprassion *value)
{
    expr = value;
}

IfElseStatement::IfElseStatement(Exprassion *expr, Statement *ifStatement, Statement *elseStatement){
    this->expr = expr;
    this->ifStatement = ifStatement;
    this->elseStatement = elseStatement;
}

void IfElseStatement::execute(){
    double exprResult = expr->eval()->asNumber();
    if(exprResult != 0){
        ifStatement->execute();
    }
    else if(elseStatement != nullptr){
        elseStatement->execute();
    }

}

void IfElseStatement::toString(QTreeWidget* AST,QTreeWidgetItem* father){
    QTreeWidgetItem* ifItem = new QTreeWidgetItem;
    ifItem->setText(0,"if");
    if(father != nullptr){
        father->addChild(ifItem);
    }
    else{
        AST->addTopLevelItem(ifItem);
    }
    expr->toString(AST,ifItem);
    ifStatement->toString(AST,ifItem);
    if(elseStatement != nullptr){
        QTreeWidgetItem* elseItem = new QTreeWidgetItem;
        elseItem->setText(0,"else");
        if(father != nullptr){
            father->addChild(elseItem);
        }
        else{
            AST->addTopLevelItem(elseItem);
        }
        elseStatement->toString(AST,elseItem);
    }
}
