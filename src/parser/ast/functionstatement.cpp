#include "functionstatement.h"

FunctionExpression *FunctionStatement::getFunc() const
{
    return (FunctionExpression*)func;
}

void FunctionStatement::setFunc(Exprassion *value)
{
    func = (FunctionExpression*)value;
}

FunctionStatement::FunctionStatement(FunctionExpression *func){
    this->func = func;
}

void FunctionStatement::execute(){
    func->eval();
}

void FunctionStatement::toString(QTreeWidget *AST, QTreeWidgetItem *father){
    QTreeWidgetItem* funcItem = new QTreeWidgetItem;
    funcItem->setText(0,"func");
    if(father != nullptr){
        father->addChild(funcItem);
    }
    else{
        AST->addTopLevelItem(funcItem);
    }
    func->toString(AST,funcItem);
}
