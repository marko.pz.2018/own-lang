#include "pressthread.h"


pressThread::pressThread(QList<Value *> args){
    this->args = args;
}

void pressThread::run(){
    int keyCode;
    foreach(Value* arg , args){
        Sleep(2000);
        keyCode = arg->asNumber();
        keybd_event(keyCode,0,0,0);
        Sleep(2000);
    }
    qDebug("Press end");
}
