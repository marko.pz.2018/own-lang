#include "variableexpression.h"



VariableExpression::VariableExpression(QString name){
    this->name = name;
}

Value* VariableExpression::eval(){
    if(Variables::isExist(name)){
        return new NumberValue(Variables::get(name));
    }
    throw "Undefined var";
}

void VariableExpression::toString(QTreeWidget* AST,QTreeWidgetItem* father){
    QTreeWidgetItem* item = new QTreeWidgetItem;
    item->setText(0,name);
    if(father != nullptr){
        father->addChild(item);
    }
    else{
        AST->addTopLevelItem(item);
    }
}
