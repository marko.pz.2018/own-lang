#pragma once
#ifndef BINARYEXPRASION_H
#define BINARYEXPRASION_H
#include "exprassion.h"

class BinaryExprassion : public Exprassion
{
private:
    Exprassion *expr1,*expr2;
    char operation;
public:
    BinaryExprassion(char operation, Exprassion* expr1,Exprassion*expr2);
    Value* eval() override;


    // Exprassion interface
public:
    void toString(QTreeWidget*,QTreeWidgetItem*);

    // Node interface
public:
    Node* accept(Visitor* visitor){
       return visitor->visit(this);
    }
    Exprassion *getExpr1() const;
    Exprassion *getExpr2() const;
    void setExpr1(Exprassion *value);
    void setExpr2(Exprassion *value);
};
#endif // BINARYEXPRASION_H
