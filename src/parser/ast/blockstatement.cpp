#include "blockstatement.h"



QList<Statement *> BlockStatement::getStatements() const
{
    return statements;
}

BlockStatement::BlockStatement()
{

}

void BlockStatement::execute(){
    foreach(Statement* tmp,statements){
        tmp->execute();
    }
}

void BlockStatement::add(Statement *statement){
    statements.push_back(statement);
}

void BlockStatement::toString(QTreeWidget* AST, QTreeWidgetItem* father){
    if(father != nullptr){
        foreach(Statement* stm , statements){
            stm->toString(AST,father);
        }
    }
    else{
        foreach(Statement* stm , statements){
            stm->toString(AST);
        }
    }
}
