#pragma once
#ifndef WHILESTATEMENT_H
#define WHILESTATEMENT_H

#include "exprassion.h"
#include "statement.h"
#include "breakstatement.h"
class WhileStatement : public Statement
{
private:
    Exprassion* condition;
    Statement* statement;
public:
    WhileStatement(Exprassion* condition,Statement* statement);

    // Statement interface
public:
    void execute();
    void toString(QTreeWidget*,QTreeWidgetItem*);

    // Node interface
    Node* accept(Visitor* visitor){
       return visitor->visit(this);
    }
    Exprassion *getCondition() const;
    Statement *getStatement() const;
    void setCondition(Exprassion *value);
    void setStatement(Statement *value);
};

#endif // WHILESTATEMENT_H
