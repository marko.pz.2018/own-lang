#pragma once
#ifndef BLOCKSTATEMENT_H
#define BLOCKSTATEMENT_H
#include "statement.h"
#include <QList>
class BlockStatement : public Statement
{
private:
    QList<Statement*> statements;
public:
    BlockStatement();

    // Statement interface
public:
    void execute();
    void add(Statement* statement);
    void toString(QTreeWidget*,QTreeWidgetItem*);


//     Node interface
    Node* accept(Visitor* visitor){
        return visitor->visit(this);
    }


    QList<Statement *> getStatements() const;
};

#endif // BLOCKSTATEMENT_H
