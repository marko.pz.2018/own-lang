#pragma once
#ifndef PRESSFUNCTION_H
#define PRESSFUNCTION_H
#include "functions.h"
#include "function.h"
#include <QtDebug>
#include <windows.h>
#include "pressthread.h"


class PressFunction: public Function
{
public:
    PressFunction();
    // Function interface
    Value *execute(QList<Value*> args);
};

#endif // PRESSFUNCTION_H
