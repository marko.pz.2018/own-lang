#pragma once
#ifndef RUNFUNCTION_H
#define RUNFUNCTION_H
#include "functions.h"
#include "function.h"
#include <QDebug>
#include <QProcess>

class RunFunction : public Function
{
public:
    RunFunction();

    // Function interface
public:
    Value *execute(QList<Value*> args);
};

#endif // RUNFUNCTION_H
