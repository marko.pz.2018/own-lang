#include "numberexprassion.h"


NumberExprassion::NumberExprassion(double value){
    this->value = value;
}

Value* NumberExprassion::eval(){
    return new NumberValue(value);
}

void NumberExprassion::toString(QTreeWidget* AST, QTreeWidgetItem* father){
    QTreeWidgetItem* item = new QTreeWidgetItem;
    item->setText(0,QString::number(value));
    if(father != nullptr){
          father->addChild(item);
     }
    else{
        AST->addTopLevelItem(item);
    }
}
