#include "whilestatement.h"


Exprassion *WhileStatement::getCondition() const
{
    return condition;
}

Statement *WhileStatement::getStatement() const
{
    return statement;
}

void WhileStatement::setCondition(Exprassion *value)
{
    condition = value;
}

void WhileStatement::setStatement(Statement *value)
{
    statement = value;
}

WhileStatement::WhileStatement(Exprassion *condition, Statement *statement){
    this->condition = condition;
    this->statement = statement;
}

void WhileStatement::execute(){
    while(condition->eval()->asNumber() != 0){
        try {
            statement->execute();
        } catch (BreakStatement bs) {
            break;
        }
    }
}

void WhileStatement::toString(QTreeWidget* AST,QTreeWidgetItem* father){
    QTreeWidgetItem* whileItem = new QTreeWidgetItem;
    whileItem->setText(0,"while");
    if(father != nullptr){
        father->addChild(whileItem);
    }
    AST->addTopLevelItem(whileItem);
    condition->toString(AST,whileItem);
    statement->toString(AST,whileItem);
}
