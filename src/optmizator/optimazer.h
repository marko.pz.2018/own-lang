#ifndef OPTIMAZER_H
#define OPTIMAZER_H

#include "parser/ast/statement.h"
#include "constantfolding.h"
#include "deadcodeelimination.h"
class Optimazer
{
public:
    Optimazer();
    Statement* optimaze(Statement* notOptimazedProgram);
};

#endif // OPTIMAZER_H
