#pragma once
#ifndef VISITOR_H
#define VISITOR_H
#include <QDebug>

class AssignmentStatement;
class IfElseStatement;
class BlockStatement;
class FunctionStatement;
class FunctionExpression;
class BinaryExprassion;
class ConditionalExprassion;
class UnaryExprassion;
class VariableExpression;
class WhileStatement;
class Node;

class Visitor
{
public:
    Visitor();
    virtual Node* visit(AssignmentStatement* st) = 0;
    virtual Node* visit(IfElseStatement* st) = 0;
    virtual Node* visit(BlockStatement* st) = 0;
    virtual Node* visit(WhileStatement* st) = 0;
    virtual Node* visit(FunctionStatement* st) = 0;
    virtual Node* visit(FunctionExpression* st) = 0;
    virtual Node* visit(BinaryExprassion* st) = 0;
    virtual Node* visit(ConditionalExprassion* st) = 0;
    virtual Node* visit(VariableExpression* st) = 0;
    virtual Node* visit(UnaryExprassion* st) = 0;

};

#endif // VISITOR_H
