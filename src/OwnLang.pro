QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

INCLUDEPATH += $$_PRO_FILE_PWD_

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    lexer/lexer.cpp \
    lexer/token.cpp \
    mainwindow.cpp \
    optmizator/constantfolding.cpp \
    optmizator/deadcodeelimination.cpp \
    optmizator/optimazer.cpp \
    optmizator/visitor.cpp \
    parser/ast/assignmentstatement.cpp \
    parser/ast/binaryexprassion.cpp \
    parser/ast/blockstatement.cpp \
    parser/ast/breakstatement.cpp \
    parser/ast/clickfunction.cpp \
    parser/ast/clickthread.cpp \
    parser/ast/conditionalexprassion.cpp \
    parser/ast/dragfunction.cpp \
    parser/ast/dragthread.cpp \
    parser/ast/exprassion.cpp \
    parser/ast/function.cpp \
    parser/ast/functionexpression.cpp \
    parser/ast/functions.cpp \
    parser/ast/functionstatement.cpp \
    parser/ast/ifelsestatement.cpp \
    parser/ast/node.cpp \
    parser/ast/numberexprassion.cpp \
    parser/ast/pressfunction.cpp \
    parser/ast/pressthread.cpp \
    parser/ast/runfunction.cpp \
    parser/ast/statement.cpp \
    parser/ast/stringexprassion.cpp \
    parser/ast/unaryexprassion.cpp \
    parser/ast/value.cpp \
    parser/ast/variableexpression.cpp \
    parser/ast/variables.cpp \
    parser/ast/whilestatement.cpp \
    parser/parser.cpp \





HEADERS += \
    mainwindow.h \
    lexer/lexer.h \
    lexer/token.h \
    optmizator/constantfolding.h \
    optmizator/deadcodeelimination.h \
    optmizator/optimazer.h \
    optmizator/visitor.h \
    parser/ast/assignmentstatement.h \
    parser/ast/binaryexprassion.h \
    parser/ast/blockstatement.h \
    parser/ast/breakstatement.h \
    parser/ast/clickfunction.h \
    parser/ast/clickthread.h \
    parser/ast/conditionalexprassion.h \
    parser/ast/dragfunction.h \
    parser/ast/dragthread.h \
    parser/ast/exprassion.h \
    parser/ast/function.h \
    parser/ast/functionexpression.h \
    parser/ast/functions.h \
    parser/ast/functionstatement.h \
    parser/ast/ifelsestatement.h \
    parser/ast/node.h \
    parser/ast/numberexprassion.h \
    parser/ast/pressfunction.h \
    parser/ast/pressthread.h \
    parser/ast/runfunction.h \
    parser/ast/statement.h \
    parser/ast/stringexprassion.h \
    parser/ast/unaryexprassion.h \
    parser/ast/value.h \
    parser/ast/variableexpression.h \
    parser/ast/variables.h \
    parser/ast/whilestatement.h \
    parser/parser.h \


FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    OwnLang.pro.user
