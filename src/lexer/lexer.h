#pragma once
#ifndef LEXER_H
#define LEXER_H
#include <QList>
#include <QString>
#include <QDebug>
#include "token.h"
class Lexer
{
public:
    QVector<QString> keywords = {"if","else","while","break","continue","click","keyPress","drag","runBatFile"};
    QList<Token> allTokens;
    QList<Token> tokanize();
    Lexer(QString sourceCode);
    int expr();

private:
    QString programText;
    int pos = 0;
    QChar curSymb = programText[pos];
    Token* curToken;
    void advance();
    Token getNextToken();
    QString tokanizeNumber();
    QString tokanizeVar();
    QString tokanizeUrl();
    void tokanizeComment();
    void tokanizeMultiComment();
    void skipWhitespace();
};

#endif // LEXER_H
