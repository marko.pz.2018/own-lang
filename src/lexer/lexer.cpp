#include "lexer.h"

Token Lexer::getNextToken()
{
    while(pos < programText.length()){
        curSymb = programText.at(pos);
        QChar symb = curSymb;
        if(curSymb.isSpace()){
            skipWhitespace();
            continue;
        }


        else if(curSymb.isDigit()){
            QString result = tokanizeNumber();
            return Token(Token::tokenType::DIGIT,result);
        }


        else if(curSymb.isLetter()){
            if(curToken->getType() == Token::tokenType::QUOTES){
                QString result = tokanizeUrl();
                return Token(Token::tokenType::URL,result);
            }
            else{
                QString result = tokanizeVar();
                if(keywords.contains(result)){
                    if(result == "if"){
                        return Token(Token::tokenType::IF,result);
                    }

                    else if(result == "else"){
                        return Token(Token::tokenType::ELSE,result);
                    }

                    else if(result == "while"){
                        return Token(Token::tokenType::WHILE,result);
                    }

                    else if(result == "break"){
                        return Token(Token::tokenType::BREAK,result);
                    }

                    else if(result == "click"){
                        return Token(Token::tokenType::CLICK,result);
                    }

                    else if(result == "keyPress"){
                        return Token(Token::tokenType::PRESS,result);
                    }

                    else if(result == "drag"){
                        return Token(Token::tokenType::DRAG,result);
                    }

                    else if(result == "runBatFile"){
                        return Token(Token::tokenType::RUN,result);
                    }

                }
                return Token(Token::tokenType::VAR,result);
            }
        }
        else if(curSymb == '+'){
            advance();
            return Token(Token::tokenType::PLUS,symb);
        }
        else if(curSymb == '-'){
            advance();
            return Token(Token::tokenType::MINUS,symb);
        }
        else if(curSymb == '*'){
            advance();
            return Token(Token::tokenType::MUL,symb);
        }
        else if(curSymb == '/'){
            advance();
            return Token(Token::tokenType::DIV,symb);
        }
        else if(curSymb == '('){
            advance();
            return Token(Token::tokenType::LBRACKET,symb);
        }

        else if(curSymb == ')'){
            advance();
            return Token(Token::tokenType::RBRACKET,symb);
        }

        else if(curSymb == ':'){
            advance();
            return Token(Token::tokenType::SEPARATORDP,symb);
        }


        else if(curSymb == ';'){
            advance();
            return Token(Token::tokenType::SEPARATORPC,symb);
        }

        else if(curSymb == ','){
            advance();
            return Token(Token::tokenType::COMA,symb);
        }

        else if(curSymb == '{'){
            advance();
            return Token(Token::tokenType::LBRACE,symb);
        }

        else if(curSymb == '}'){
            advance();
            return Token(Token::tokenType::RBRACE,symb);
        }

        else if(curSymb == "\""){
            advance();
            return Token(Token::tokenType::QUOTES,symb);
        }


        else if(curSymb == '>'){
            advance();
            return Token(Token::tokenType::GS,symb);
        }


        else if(curSymb == '<'){
            advance();
            return Token(Token::tokenType::LS,symb);
        }

        else if(curSymb == '='){
            advance();
            return Token(Token::tokenType::EQUALLY,symb);
        }

        else if(curSymb == '#'){
            advance();
            if(curSymb == '#'){
                advance();
                tokanizeMultiComment();
            }
            else{
            tokanizeComment();
            }
        }

        else{
            throw "Invalid symbol";
        }
    }
    if(pos >= programText.length()){
        return Token(Token::tokenType::EoF,"\0");
    }
}

void Lexer::advance()
{
    pos++;
    if(pos > programText.length() - 1){
        curSymb = '\0';
    }
    else{
        curSymb = programText[pos];
    }
}





QString Lexer::tokanizeNumber()
{
    QString result = "";
    while(true){
        if(curSymb.isDigit() || (curSymb == '.' && result.indexOf('.') == -1)){
            result += curSymb;
            advance();
            continue;
        }
        break;
    }
    return result;
}

QString Lexer::tokanizeVar()
{
    QString result = "";
    while(curSymb != ':' && curSymb.isLetterOrNumber()){
        result += curSymb;
        advance();
    }
    return result;
}

QString Lexer::tokanizeUrl()
{
    QString result = "";
    while(curSymb != '"' && (curSymb.isLetterOrNumber() || curSymb == '/'
                             || curSymb == '\\' || curSymb == ':' || curSymb == '.'
                             || curSymb == ' ')){
        result += curSymb;
        advance();
    }
    return result;

}

void Lexer::tokanizeComment()
{
    while(true){
        if(curSymb == "\n" || curSymb == '\0'){
            break;
        }
        advance();
    }
}

void Lexer::tokanizeMultiComment()
{
    while(true){
        if(curSymb == '\0'){
            throw std::runtime_error("Missing close tag");
        }

        if(curSymb == '#'){
            advance();
            if(curSymb == '#'){
                advance();
                break;
            }
        }
        advance();
    }
}

void Lexer::skipWhitespace()
{
    while(curSymb != '\0' && curSymb.isSpace())
        advance();
}



QList<Token> Lexer::tokanize()
{
    do{
      allTokens.append(getNextToken());
      curToken = &(allTokens.last());
    }while(allTokens.last().getType() != Token::tokenType::EoF);
    return allTokens;
}

Lexer::Lexer(QString sourceCode){
    programText = sourceCode;
    curToken = new Token(Token::tokenType::EoF,"\0");
}

