#pragma once
#ifndef TOKEN_H
#define TOKEN_H
#include <QString>



class Token
{
public:
    enum class tokenType{
      DIGIT,
      URL,
      VAR,
      IF,
      ELSE,
      WHILE,
      CLICK,
      PRESS,
      DRAG,
      RUN,
      PLUS,
      LS,
      GS,
      EQUALLY,
      MINUS,
      MUL,
      DIV,
      SEPARATORDP,
      SEPARATORPC,
      COMA,
      OPERATOR,
      RBRACKET,
      LBRACKET,
      LBRACE,
      RBRACE,
      QUOTES,
      BREAK,
      COMMENT,
      EoF
    };
private:
    QString value;
    tokenType type;
    QString typeToString();
public:
    Token(Token::tokenType tokenType,QString val);
    QString toString();
    QString getValue();
    Token::tokenType getType();
};

#endif // TOKEN_H
